import csv
import scipy.stats

# Read mutation database
def read_mutation_database(filename):
    mutations_dict = {}
    
    with open(filename, 'r') as file:
        for line in file:
            parts = line.strip().split(';')
            lineage = parts[0]
            mutations = parts[1].split(',')
            mutations_dict[lineage] = set(mutations)
    
    return mutations_dict

# Read final_nextclade.csv
def read_nextclade_csv(filename):
    asv_mutations = {}
    
    with open(filename, 'r') as file:
        csv_reader = csv.reader(file, delimiter=';')  # Specify the delimiter as ';'
        next(csv_reader)  # Skip header
        for row in csv_reader:
            asv_id = row[0]
            mutations = row[1].split(',')
            asv_mutations[asv_id] = set(mutations)
    
    return asv_mutations

# Calculate hypergeometric p-value
def calculate_hypergeometric_pvalue(K, N, n, k):
    p_value = scipy.stats.hypergeom.sf(k - 1, N, K, n)
    return p_value

# Assign lineages to ASVs based on mutation comparison and calculate p-values
def assign_lineages(mutations_dict, asv_mutations):
    assigned_lineages = {}

    for asv_id, asv_mutations_set in asv_mutations.items():
        best_lineage = None
        best_overlap = 0
        best_p_value = 1.0

        for lineage, lineage_mutations_set in mutations_dict.items():
            overlap = len(asv_mutations_set.intersection(lineage_mutations_set))
            K = len(lineage_mutations_set)
            n = len(asv_mutations_set)
            k = overlap
            N = total_mutations  # Replace with the actual total number of mutations

            if overlap > best_overlap:
                best_overlap = overlap
                best_lineage = lineage
                best_p_value = calculate_hypergeometric_pvalue(K, N, n, k)

        assigned_lineages[asv_id] = (best_lineage, best_p_value)

    return assigned_lineages

# Read mutation database
mutation_db_filename = 'database_hypergeometric_analysis.txt' #Replace with the name of the base
mutations_dict = read_mutation_database(mutation_db_filename)

# Read final_nextclade.csv
nextclade_csv_filename = 'ASV_mutations.csv' #replace with the name of the ASV mutations file
asv_mutations = read_nextclade_csv(nextclade_csv_filename)

# Total number of mutations (replace with actual value)
total_mutations = 7709

# Assign lineages to ASVs based on mutation comparison and calculate p-values
assigned_lineages = assign_lineages(mutations_dict, asv_mutations)

# Save assigned lineages and p-values to an output TSV file
output_filename = 'assigned_lineages.tsv'
with open(output_filename, 'w', newline='') as file:
    tsv_writer = csv.writer(file, delimiter='\t')
    tsv_writer.writerow(['Feature ID', 'Taxon', 'P-value'])
    for asv_id, (lineage, p_value) in assigned_lineages.items():
        tsv_writer.writerow([asv_id, f"SARS-CoV-2; {lineage}", p_value])

